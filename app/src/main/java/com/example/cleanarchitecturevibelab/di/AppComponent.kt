package com.example.cleanarchitecturevibelab.di

import com.example.cleanarchitecturevibelab.MainActivity
import dagger.Component

@Component(modules = [NetworkModule::class])
interface AppComponent {
    fun injectMainActivity(mainActivity: MainActivity)
}