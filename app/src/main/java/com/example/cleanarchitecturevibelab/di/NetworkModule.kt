package com.example.cleanarchitecturevibelab.di

import com.example.cleanarchitecturevibelab.network.RetrofitBuilder
import dagger.Module
import dagger.Provides

@Module
class NetworkModule {

    @Provides
    fun provideRetrofitBuilder(): RetrofitBuilder {
        return RetrofitBuilder
    }
}