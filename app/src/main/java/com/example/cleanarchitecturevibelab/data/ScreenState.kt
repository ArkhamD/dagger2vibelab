package com.example.cleanarchitecturevibelab.data

enum class ScreenState {
    FactScreen, WebScreen
}