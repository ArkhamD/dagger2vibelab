package com.example.cleanarchitecturevibelab.data

data class CatFact(
    val fact: String,
    val length: Int
)