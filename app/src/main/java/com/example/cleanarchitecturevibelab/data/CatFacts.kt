package com.example.cleanarchitecturevibelab.data

data class CatFacts(
    val current_page: Int,
    val `data`: List<CatFact>
)