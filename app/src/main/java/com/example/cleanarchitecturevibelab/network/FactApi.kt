package com.example.cleanarchitecturevibelab.network

import com.example.cleanarchitecturevibelab.data.CatFacts
import retrofit2.http.GET
import retrofit2.http.Query

interface FactApi {
    @GET("facts")
    suspend fun getCatFacts(@Query("limit") limit: Int): CatFacts
}