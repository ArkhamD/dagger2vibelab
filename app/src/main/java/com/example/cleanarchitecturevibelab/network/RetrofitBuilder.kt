package com.example.cleanarchitecturevibelab.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitBuilder {
    fun getInstance(): FactApi {
        val retrofit= Retrofit.Builder()
            .baseUrl("https://catfact.ninja/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        return retrofit.create(FactApi::class.java)
    }
}