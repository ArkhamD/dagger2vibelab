package com.example.cleanarchitecturevibelab

import android.app.Application
import com.example.cleanarchitecturevibelab.di.AppComponent

class MyApplication : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.create
    }
}
